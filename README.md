# Spring Boot Kotlin JPA MySQL Restful API Demo project
This is the source code for the a sample Spring Boot application developed with Kotlin and Spring Data JPA.

You can launch the application with by running:
```
$ ./gradlew clean bootRun
```

This project uses kotlin-spring plugin to avoid requiring open modifier on proxified classes and methods, see this blog post for more details.

Make sure you have at least IntelliJ IDEA 2019.3 and Kotlin plugin 1.3.71 since kotlin-spring and kotlin-jpa compiler plugins require this version.

This project uses a Kotlin based Gradle configuration, but build.gradle.kts auto-complete is currently not available since Kotlin 1.2-71 IDEA plugin needed for that does not allow to use kotlin-spring and kotlin-jpa compiler in a reliable way.

This project is Apache 2.0 licensed.

## Screen shot

Home Page

![Home Page](img/home.png "Home Page")

List Customers

![List Customers](img/list.png "List Customers")
