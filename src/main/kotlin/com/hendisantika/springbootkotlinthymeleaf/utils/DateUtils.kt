package com.hendisantika.springbootkotlinthymeleaf.utils

import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-kotlin-thymeleaf
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 20/03/18
 * Time: 20.49
 * To change this template use File | Settings | File Templates.
 */


enum class DateOptUnit {
    YEAR, MONTH, DATE;

    fun parseType(): Int {
        var value = Calendar.DATE
        when (this) {
            YEAR -> value = Calendar.DATE
            MONTH -> value = Calendar.MONTH
            DATE -> value = Calendar.DATE
        }
        return value
    }
}

data class DateOperator(val unit: DateOptUnit, val value: Int)

fun Any.year(value: Int): DateOperator {
    return DateOperator(DateOptUnit.YEAR, value)
}

fun Any.month(value: Int): DateOperator {
    return DateOperator(DateOptUnit.MONTH, value)
}

fun Any.day(value: Int): DateOperator {
    return DateOperator(DateOptUnit.DATE, value)
}

operator fun Date.plus(nextVal: Int): Date {
    val calendar = GregorianCalendar()
    calendar.time = this
    calendar.add(Calendar.DATE, nextVal)
    return calendar.time
}

operator fun Date.minus(nextVal: Int): Date {
    val calendar = GregorianCalendar()
    calendar.time = this
    calendar.add(Calendar.DATE, nextVal * -1)
    return calendar.time
}

operator fun Date.plus(nextVal: DateOperator): Date {
    val calendar = GregorianCalendar()
    calendar.time = this
    calendar.add(nextVal.unit.parseType(), nextVal.value)
    return calendar.time
}

operator fun Date.minus(nextVal: DateOperator): Date {
    val calendar = GregorianCalendar()
    calendar.time = this
    calendar.add(nextVal.unit.parseType(), nextVal.value * -1)
    return calendar.time
}

operator fun Date.inc(): Date {
    val calendar = GregorianCalendar()
    calendar.time = this
    calendar.add(Calendar.MONTH, 1)
    calendar.set(Calendar.DAY_OF_MONTH, 0)
    return calendar.time
}

operator fun Date.dec(): Date {
    val calendar = GregorianCalendar()
    calendar.time = this
    calendar.set(Calendar.DAY_OF_MONTH, 1)
    return calendar.time
}

operator fun Date.get(position: Int): Int {
    val calendar = GregorianCalendar()
    calendar.time = this
    var value = 0
    when (position) {
        0 -> value = calendar.get(Calendar.YEAR)
        1 -> value = calendar.get(Calendar.MONTH) + 1
        2 -> value = calendar.get(Calendar.DAY_OF_MONTH)
        3 -> value = calendar.get(Calendar.HOUR)
        4 -> value = calendar.get(Calendar.MINUTE)
        5 -> value = calendar.get(Calendar.SECOND)
    }
    return value
}

operator fun Date.compareTo(compareDate: Date): Int {
    return (time - compareDate.time).toInt()
}

fun Date.stringFormat(formatType: String): String {
    return SimpleDateFormat(formatType).format(this)
}