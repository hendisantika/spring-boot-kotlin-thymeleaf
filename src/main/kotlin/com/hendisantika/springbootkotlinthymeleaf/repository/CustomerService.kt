package com.hendisantika.springbootkotlinthymeleaf.repository

import com.hendisantika.springbootkotlinthymeleaf.entity.Customer
import org.springframework.data.repository.CrudRepository

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-kotlin-thymeleaf
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 20/03/18
 * Time: 20.45
 * To change this template use File | Settings | File Templates.
 */
interface CustomerService : CrudRepository<Customer, Long> {

    fun findByLastName(lastName: String): Iterable<Customer>
}