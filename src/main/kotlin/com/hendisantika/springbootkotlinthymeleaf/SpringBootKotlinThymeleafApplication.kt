package com.hendisantika.springbootkotlinthymeleaf

import com.hendisantika.springbootkotlinthymeleaf.entity.Customer
import com.hendisantika.springbootkotlinthymeleaf.repository.CustomerService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.Bean
import java.util.*

@SpringBootApplication
class SpringBootKotlinThymeleafApplication {

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            SpringApplication.run(SpringBootKotlinThymeleafApplication::class.java, *args)
        }
    }

    @Autowired
    lateinit var customerService: CustomerService

    private val log = LoggerFactory.getLogger(SpringBootKotlinThymeleafApplication::class.java)

    @Bean
    fun init(repository: CustomerService) = CommandLineRunner {
//    fun init() = CommandLineRunner {
        // save a couple of customers
        val now = Date()
        customerService.save(Customer("Uzumaki", "Naruto", now, now, 0, now))
        customerService.save(Customer("Uchiha", "Sasuke", now, now, 0, now))
        customerService.save(Customer("Haruno", "Sakura", now, now, 0, now))
        customerService.save(Customer("Hatake", "Kakashi", now, now, 0, now))
        customerService.save(Customer("Hendi", "Santika", now, now, 0, now))

        for (customer in customerService.findAll()) {
            log.info(customer.toString())
        }

        // fetch an individual customer by ID
        val customer = customerService.findById(1L)
        log.info(customer.toString())
        // fetch customers by last name
        for (data in customerService.findByLastName("Naruto")) {
            log.info(data.toString())
        }
    }


}
