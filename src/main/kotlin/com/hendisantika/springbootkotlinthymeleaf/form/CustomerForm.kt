package com.hendisantika.springbootkotlinthymeleaf.form

import java.util.*

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-kotlin-thymeleaf
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 05/04/20
 * Time: 21.43
 */
class CustomerForm {
    var id: Long = 1
    var firstName: String? = null
    var lastName: String? = null
    var gmtCreated: Date? = Date()
    var gmtModified: Date? = Date()
    var isDeleted: Int? = 0 //1 Yes 0 No
    var deletedDate: Date? = Date()
}