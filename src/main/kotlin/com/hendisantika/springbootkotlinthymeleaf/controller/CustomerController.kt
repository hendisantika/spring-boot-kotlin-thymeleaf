package com.hendisantika.springbootkotlinthymeleaf.controller

import com.hendisantika.springbootkotlinthymeleaf.form.CustomerForm
import com.hendisantika.springbootkotlinthymeleaf.repository.CustomerService
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.ResponseBody

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-kotlin-thymeleaf
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 21/03/18
 * Time: 22.01
 * To change this template use File | Settings | File Templates.
 */
@Controller
class CustomerController(val customerService: CustomerService) {

    @GetMapping("/")
    fun index(): String {
        return "index"
    }

    @GetMapping("/customers")
    fun listAll(model: Model): String {
        val allCustomers = customerService.findAll()
        model.addAttribute("customers", allCustomers)
        return "customers"
    }

    @GetMapping("/customer/new")
    fun createCustomer(model: Model): String {
        val customer = CustomerForm()
        model.addAttribute("customer", customer)
        return "customers"
    }

    @GetMapping("/listCustomers")
    @ResponseBody
    fun listCustomers(model: Model) = customerService.findAll()

    @GetMapping("/{lastName}")
    @ResponseBody
    fun findByLastName(@PathVariable lastName: String) = customerService.findByLastName(lastName)
}