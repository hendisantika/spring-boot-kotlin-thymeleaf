package com.hendisantika.springbootkotlinthymeleaf.entity

import java.util.*
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-kotlin-thymeleaf
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 20/03/18
 * Time: 06.32
 * To change this template use File | Settings | File Templates.
 */
@Entity
data class Customer(
        val firstName: String,
        val lastName: String,
        val gmtCreated: Date,
        val gmtModified: Date,
        val isDeleted: Int, //1 Yes 0 No
        val deletedDate: Date) {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    var id: Long = -1

    constructor(
            id: Long,
            fn: String,
            ln: String,
            created: Date,
            modified: Date,
            isdeleted: Int, //1 Yes 0 No
            deleted: Date
    ) : this(fn, ln, created, modified, isdeleted, deleted) {
        this.id = id
    }

    override fun toString(): String {
        return "Customer(firstName='$firstName', " +
                "lastName='$lastName', " +
                "gmtCreated=$gmtCreated, " +
                "gmtModified=$gmtModified, " +
                "isDeleted=$isDeleted, " +
                "deletedDate=$deletedDate, id=$id)"
    }

}