$(function () {
    var aLengthMenu = [5, 7, 10, 20, 50, 100, 200];
    var dataTableOptions = {
        "bDestroy": true,
        dom: 'lfrtip',
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "processing": true,
        "stateSave": true,
        responsive: true,
        fixedHeader: false,
        order: [[0, "asc"]],
        "aLengthMenu": aLengthMenu,
        language: {
            "search": "<div style='border-radius:10px;margin-left:auto;margin-right:2px;width:760px;'>_INPUT_ &nbsp;<span class='btn btn-primary'>Search</span></div>",

            paginate: {//Pagination style content
                previous: "Previous page",
                next: "Next page",
                first: "First page",
                last: "Last"
            }
        },
        zeroRecords: "No content",//When the content of table tbody is empty, the content of tbody.
        //The following three constitute the overall content in the lower left corner.
        info: "_TOTAL_ total, _PAGES_ pages, _START_-_END_ ",//The information in the lower left corner shows that capitalized words are keywords.
        infoEmpty: "0 records",//The display in the lower left corner when the filter is empty.
        infoFiltered: ""//Screening tips in the lower left corner after screening
    };

    $('#customersTable').dataTable(dataTableOptions)
});